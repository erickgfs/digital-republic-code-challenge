import Modal from "react-modal";
import styles from "./styles.module.scss";
import { ModalProps } from "../../pages/index";

import { FiX } from 'react-icons/fi';

interface ModalPopsInfo {
    isOpen: boolean;
    onRequestClose: () => void;
    infos: ModalProps;
    paredes: number[];
}

export function ModalResponse({infos, isOpen, onRequestClose, paredes}: ModalPopsInfo){
    const customStyles = {
        content: {
            top: "50%",
            buttom: "auto",
            left: "50%",
            right: "auto",
            padding: "30px",
            transform: "translate(-50%, -50%)",
            backgroundColor: "#222831",
            height: "500px"
        }
    }
    return(
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            style={customStyles}
        >
            <button type="button" onClick={onRequestClose} className="react-modal-close" style={{background: 'transparent', border: 0}}>
                <FiX size={45} color="#F34748"/>
            </button>

            <div className={styles.container}>
                <h2>Metros quadrados por parede:</h2>
                { paredes.map((parede, index) => (
                    <p>Parede {index + 1}: {parede} m²</p>
                ))}
                <p>Total: {paredes.reduce((previousValue, currentValue) => previousValue + currentValue, 0)} m²</p>
                
                <h2>Quantidade de latas necessárias:</h2>
                <p>0,5 Litros: {infos.meioLitro}</p>
                <p>2,5 Litros: {infos.doisLitrosMeio}</p>
                <p>3,6 Litros: {infos.tresLitrosSeiscentos}</p>
                <p>18 Litros: {infos.dezoitoLitros}</p>
            </div>
        </Modal>
    )
}