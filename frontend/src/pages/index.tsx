import { useState, FormEvent } from "react";
import { toast } from "react-toastify";
import Modal from "react-modal";
import { ModalResponse } from "../components/ModalResponse";

import { api } from "../services/apiClient";

import Head from "next/head";
import Image from "next/image";

import "../../styles/home.module.scss";
import styles from "../../styles/home.module.scss";

import Logo from "../../public/mainLogo.png";

import { Input } from "../components/ui/Input";
import { Button } from "../components/ui/Button";

export type ModalProps = {
  meioLitro: number;
  doisLitrosMeio: number;
  tresLitrosSeiscentos: number;
  dezoitoLitros: number;
}

export default function Home() {
  const [parede, setParede] = useState<number[]>([]);
  const [index, setIndex] = useState(0);
  const [altura, setAltura] = useState("");
  const [largura, setLargura] = useState("");
  const [portas, setPortas] = useState("");
  const [janelas, setJanelas] = useState("");
  const [modalItem, setModalItem] = useState<ModalProps>({
    meioLitro: 0,
    doisLitrosMeio: 0,
    tresLitrosSeiscentos: 0,
    dezoitoLitros: 0,
  });
  const [modalVisible, setModalVisible] = useState(false);

  function handleCloseModal() {
    setParede([]);
    setAltura("");
    setLargura("");
    setPortas("");
    setJanelas("");
    setIndex(0);
    setModalVisible(false);
    console.log(parede)
  }

  function validations(metrosQuadrados: number, metrosQuadradosJanelas: number, metrosQuadradosPortas: number) {
    if(metrosQuadrados < 1) {
      toast.error("Parede com menos de 1 metro quadrados!");
      return false;
    } else if (metrosQuadrados > 50) {
      toast.error("Parede com mais de 50 metros quadrados!");
      return false;
    } else if ((metrosQuadrados / 2) < (metrosQuadradosJanelas + metrosQuadradosPortas)) {
      toast.error("Janelas e portas excedem 50% da area desta parede!");
      return false;
    }else if (Number(altura) < 2.20 && metrosQuadradosPortas > 0) {
      toast.error("Portas só podem ser colocadas em paredes superiores a 2,20 de altura!");
      return false;
    }
    return true;
  }

  async function calcularParede(e: FormEvent) {
    e.preventDefault();

    let metrosQuadrados = Number(altura) * Number(largura);
    let metrosQuadradosJanelas = (2 * 1.20) * Number(janelas);
    let metrosQuadradosPortas = (0.8 * 1.90) * Number(portas);

    let isOK = validations(metrosQuadrados, metrosQuadradosJanelas, metrosQuadradosPortas);
    if(!isOK) {
      return;
    }

    metrosQuadrados = metrosQuadrados - metrosQuadradosJanelas - metrosQuadradosPortas;
    parede.push(metrosQuadrados);
    
    if(index == 3) {      
      const response = await api.post('/calcLatas', { parede });

      setModalItem(response.data);

      console.log('response', response)
      
      setModalVisible(true);
      return;
    }
    
    setAltura("");
    setLargura("");
    setPortas("");
    setJanelas("");
    setIndex(index + 1);
  } 

  function retornar(e: FormEvent) {
    e.preventDefault();
    if (index < 0) {
      return;
    }
    parede.pop();
    setAltura("");
    setLargura("");
    setPortas("");
    setJanelas("");
    setIndex(index - 1)
  } 

  Modal.setAppElement("#__next");
  
  return (
    <>
      <Head>
        <title>
          Ink Calculator
        </title>
      </Head>
      <div className={styles.containerCenter}>
        <Image src={Logo} alt="Logo Principal" />
        <div>
          <form className={styles.form}>
            <h1>Parede {index + 1}</h1>
            <div>
              <div className={styles.sides}>
                <div>
                  <Input 
                    type="number" 
                    min="0"
                    placeholder="Largura (m)" 
                    value={largura} 
                    onChange={(e) => setLargura(e.target.value)}
                  />
                  <Input 
                    type="number" 
                    min="0"
                    placeholder="Altura (m)" 
                    value={altura} 
                    onChange={(e) => setAltura(e.target.value)}
                  />
                </div>
                <div>
                  <Input 
                    type="number" 
                    min="0"
                    placeholder="Portas" 
                    value={portas} onChange={(e) => setPortas(e.target.value)}
                  />
                  <Input 
                    type="number" 
                    min="0"
                    placeholder="Janelas" 
                    value={janelas} 
                    onChange={(e) => setJanelas(e.target.value)}
                  />
                </div>
              </div>
            </div>
            { index == 3 ? (
                <Button type="submit" onClick={calcularParede}>Calcular</Button>
              ) : (
                index == 0 ? (
                  <Button type="submit"  onClick={calcularParede}>Próximo</Button>
                ) : (
                  <div className={styles.buttonsContainer}>
                    <Button type="submit" onClick={retornar}>Anterior</Button>
                    <Button type="submit" onClick={calcularParede}>Próximo</Button>
                  </div>
                )
              )}
          </form>
        </div>
        {modalVisible && (
          <ModalResponse 
            isOpen={modalVisible}
            onRequestClose={handleCloseModal}
            infos={modalItem}
            paredes={parede}
          />
        )}
      </div>
    </>
  )
}
