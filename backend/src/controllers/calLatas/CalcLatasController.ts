import {Request, response, Response} from 'express';
import { CalcLatasService } from '../../services/CalcLatasService';

class CalcLatasController {
    async handle(req: Request, res: Response) {
        const { parede } = req.body;
        console.log(req.body)

        const calcLatasService = new CalcLatasService();
        
        const result = await calcLatasService.execute({parede});

        return res.json(result);
    }
}

export { CalcLatasController }