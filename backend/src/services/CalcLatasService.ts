interface CalcLatasRequest {
    parede: [];
}

class CalcLatasService {
    async execute({ parede }: CalcLatasRequest) {
        console.log(parede)

        var totalMetrosQuadrados = parede.reduce((previousValues, currentValue) => previousValues + currentValue, 0);
        const metrosQuadradosCemMls = 5 / 10;

        const metrosQuadradosMeioL = 5 / 2;
        const metrosQuadrados2L5ml = (5 * 2) + metrosQuadradosMeioL;
        const metrosQuadrados3L6ml = (5 * 3) + (metrosQuadradosCemMls * 6);
        const metrosQuadrados18L = 5 * 18;

        var meioLitro = 0;
        var doisLitrosMeio = 0;
        var tresLitrosSeiscentos = 0;
        var dezoitoLitros = 0;
        
        while(totalMetrosQuadrados > 0) {
            if(metrosQuadrados18L < totalMetrosQuadrados) {
                totalMetrosQuadrados -= metrosQuadrados18L;
                dezoitoLitros++;
            } else if (metrosQuadrados3L6ml < totalMetrosQuadrados){
                totalMetrosQuadrados -= metrosQuadrados3L6ml;
                tresLitrosSeiscentos++;
            } else if (metrosQuadrados2L5ml < totalMetrosQuadrados){
                totalMetrosQuadrados -= metrosQuadrados2L5ml;
                doisLitrosMeio++;
            } else {
                totalMetrosQuadrados -= metrosQuadradosMeioL;
                meioLitro++;
            }
        }

        const result = {
            meioLitro,
            doisLitrosMeio,
            tresLitrosSeiscentos,
            dezoitoLitros
        }

        return result;
    }
}

export { CalcLatasService }