import { Router } from 'express';

import { CalcLatasController } from './controllers/calLatas/CalcLatasController';

const router = Router();

router.post('/calcLatas', new CalcLatasController().handle);

export { router };